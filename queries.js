const sqlite3 = require('sqlite3').verbose();

// TODO:
// Find a way to deal with this error if opening the database fails.
// Maybe send a 500 error message back
const db = new sqlite3.Database(
    "./regions.sqlite3",
    sqlite3.OPEN_READWRITE,
    (err) => {
        if (err) {
            console.log(err)
        }
    });

module.exports = {
    fetchAllRegions: function fetchAllRegions() {
        return new Promise((resolve, reject) => {
            db.serialize(() => {
                db.all("select name, capital, population from regions", (err, rows) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(rows);
                });
            });
        });
    },

    fetchOneRegion: function (id) {
        return new Promise((resolve, reject) => {
            db.serialize(() => {
                db.all("SELECT name, capital, population FROM regions where id = ?", [id], (err, row) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(row);
                });
            });
        });
    }
}