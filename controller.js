const queries = require('./queries');

// Total number of regions.
const REGIONS_COUNT = 16; 

module.exports = {
    index: function(request, reply) {
        return queries.fetchAllRegions().then(regions => {
            reply.send({
                ok: true,
                status: 200,
                summary: {
                    source: "2010 population census", // FIXME: Maybe this should come from a dynamic source.
                    totalRegions: regions.length,
                    totalPopulation: regions.reduce((total, region, ) => total += Number(region.population), 0),
                },
    
                data: regions,
            }).code(200);
    
        }).catch(err => {
            if (err) {
                console.log(err);
    
                reply.send({
                    ok: false,
                    status: 500,
                    data: null,
                    msg: "An internal error occured",
                }).code(200);
            }
        });
    },
    
    randomRegion: function (request, reply) {
        // Let's generate a random number to be used as an id for a region
        // in the database
        let regionID = Math.floor(Math.random() * REGIONS_COUNT);

        // Math.random() can return zero which we cannot use as an 
        // ID in the database. So we increment it to one
        regionID = (regionID === 0) ? ++regionID : regionID;

        queries.fetchOneRegion(regionID).then(region => {
            reply.send({
                ok: true,
                status: 200,
                data: region,
            }).code(200)
        }).catch(err => {
            if (err) {
                reply.send({
                    ok: false,
                    status: 500,
                    data: null,
                    msg: "An internal error occured"
                }).code(200);
            }
        });
    }
}
