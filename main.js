const fastify = require('fastify')({
    logger: true
});

const apiController = require("./controller");

console.log(process.env.PORT);

fastify.get('/', apiController.index);
fastify.get('/random', apiController.randomRegion);
    
const start = async () => {
    try {
        await fastify.listen(process.env.PORT || 8080);
        fastify.log.info(`server listening on ${fastify.server.address().port}`);
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
}
start();